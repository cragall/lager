-module(lager_use).
-compile([{parse_transform, lager_transform}]).

%% API
-export([start/0, rec/1, print/0, setLoglevel/0, info/0, error/0]).

start()->
  lager:start(),
  lager:debug("debug"),
  lager:info("info"),
  lager:warning("warning"),
  lager:error("error"),
  lager:emergency("emergency"),
  lager:alert("alert"),
  lager:critical("critical"),
  lager:error("hurra, dzialam").

print()->
  lager:error("--------------------------------------"),
  lager:info("info"),
  State = {er, 10},
  lager:info("My state is ~p", [lager:pr(State, ?MODULE)]).

info()->
  lager:info("info").

error()->
  lager:error("err").

setLoglevel() -> lager:set_loglevel(lager_console_backend, error).

rec(N) ->
  if
    N>0 -> lager:emergency("erlang rzadzi!!"), rec(N-1);
    true -> ok
  end.
